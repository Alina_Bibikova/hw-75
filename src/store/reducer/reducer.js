import {CREATE_DECODE_SUCCESS, DECODE, ENCODE, FETCH_ENCODE_SUCCESS, PASSWORD} from "../action/action";

const initialState = {
    password: '',
    decodeMessage: '',
    encodeMessage: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case PASSWORD: {
            return {...state, password: action.text}
        }
        case ENCODE: {
            return {...state, encodeMessage: action.text}
        }
        case DECODE: {
            return {...state, decodeMessage: action.text}
        }
        case FETCH_ENCODE_SUCCESS:
            return {...state,
                decodeMessage: action.code
            };
        case CREATE_DECODE_SUCCESS:
            return {...state,
                encodeMessage: action.code
            };

        default:
            return state;
    }
};

export default reducer;