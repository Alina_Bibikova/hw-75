import axios from '../../axios-api';

export const FETCH_ENCODE_SUCCESS = 'FETCH_ENCODE_SUCCESS';
export const CREATE_DECODE_SUCCESS = 'CREATE_DECODE_SUCCESS';
export const PASSWORD = 'PASSWORD';
export const DECODE = 'DECODE';
export const ENCODE = 'ENCODE';

export const fetchEncodeSuccess = code => ({type: FETCH_ENCODE_SUCCESS, code});
export const createDecodeSuccess = code => ({type: CREATE_DECODE_SUCCESS, code});

export const passwordHandler = text => ({type: PASSWORD, text});
export const encodeHandler = text => ({type: ENCODE, text});
export const decodeHandler = text => ({type: DECODE, text});

export const createDecode = (codeData) => {
    return dispatch => {
        return axios.post('/encode', codeData).then(
            response => dispatch(createDecodeSuccess(response.data.encode))
        );
    };
};

export const createEncode = (codeData) => {
    return dispatch => {
        return axios.post('/decode', codeData).then(
            response => dispatch(fetchEncodeSuccess(response.data.decode))
        );
    };
};
