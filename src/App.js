import React, { Component } from 'react';
import {Button, Col, Container, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {createDecode, createEncode, decodeHandler, encodeHandler, passwordHandler} from "./store/action/action";

class App extends Component {

    submitDecode = () => {
        const responseObject = {
            password: this.props.code.password,
            message: this.props.code.decodeMessage
        };
        this.props.createDecode(responseObject)
    };

    submitEncode = () => {
        const responseObject = {
            password: this.props.code.password,
            message: this.props.code.encodeMessage
        };
        this.props.createEncode(responseObject)
    };

  render() {
    return (
        <Container style={{marginTop: '20px'}}>
            <FormGroup row>
                <Label sm={2} for="decoded">Decoded message</Label>
                <Col sm={5}>
                    <Input
                        type="textarea" required
                        name="decoded"
                        placeholder="Decoded"
                        value={this.props.code.decodeMessage}
                        onChange={(e) => this.props.decodeHandler(e)}
                    />
                </Col>
            </FormGroup>
            <FormGroup row>
                    <Label sm={2} for="password">Password</Label>
                <Col sm={3}>
                    <Input
                        type="text" required
                        name="password"
                        placeholder="Password"
                        value={this.props.code.password}
                        onChange={(e) => this.props.passwordHandler(e)}
                    />
                </Col>
                <Button disabled={!this.props.code.password} onClick={this.submitEncode} sm={2} type="submit" color="info">Up</Button>
                <Button disabled={!this.props.code.password} onClick={this.submitDecode} style={{marginLeft: '10px'}} sm={2} type="submit" color="info">Down</Button>
            </FormGroup>
            <FormGroup row>
                <Label sm={2} for="encoded">Encoded message</Label>
                <Col sm={5}>
                    <Input
                        type="textarea" required
                        name="encoded"
                        placeholder="Encoded"
                        value={this.props.code.encodeMessage}
                        onChange={(e) => this.props.encodeHandler(e)}
                    />
                </Col>
            </FormGroup>
        </Container>
    );
  }
}

const mapStateToProps = state => {
    return {
        code: state.code
    };
};

const mapDispatchToProps = dispatch => {
    return {
        passwordHandler: event => dispatch(passwordHandler(event.target.value)),
        encodeHandler: event => dispatch(encodeHandler(event.target.value)),
        decodeHandler: event => dispatch(decodeHandler(event.target.value)),
        createDecode: code => dispatch(createDecode(code)),
        createEncode: code => dispatch(createEncode(code)),

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
